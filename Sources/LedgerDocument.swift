// Copyright 2023-2025 Kai Oezer

import SwiftUI
import UniformTypeIdentifiers
import Buchhaltung
import TOMLike
import IssueCollection

struct LedgerDocument
{
	var ledger : Ledger
}

extension LedgerDocument : FileDocument
{
	static var readableContentTypes: [UTType] { [.buchhaltungLedger] }
	static var writableContentTypes: [UTType] { [.buchhaltungLedger] }

	init(configuration: ReadConfiguration) throws
	{
		guard configuration.contentType == .buchhaltungLedger else {
			throw BelanzError.contentTypeNotSupported
		}
		guard let fileContents = configuration.file.regularFileContents else {
			throw CocoaError(.fileReadCorruptFile)
		}
		guard let archivedDocument = String(data: fileContents, encoding: .utf8) else {
			throw BelanzError.documentUnarchiving
		}
		var issues = IssueCollection()
		let ledger = try TOMLikeCoder.decode(Ledger.self, from: archivedDocument, issues: &issues)
		assert(issues.issues.isEmpty)
		if !issues.issues.isEmpty { logger.error("\(issues)") }
		self.init(ledger: ledger)
	}

	func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper
	{
		guard configuration.contentType == .buchhaltungLedger else {
			throw BelanzError.contentTypeNotSupported
		}
		let archive = try TOMLikeCoder.encode(self.ledger)
		guard let fileData = archive.data(using: .utf8) else {
			throw BelanzError.documentArchivingDataConversion
		}
		return .init(regularFileWithContents: fileData)
	}
}
