// Copyright 2023 Kai Oezer

import UniformTypeIdentifiers

extension UTType
{
	static var belanz : UTType
	{
		UTType(exportedAs: "belanz.belanz", conformingTo: .utf8PlainText)
	}

	static var buchhaltungLedger : UTType
	{
		UTType(importedAs: "belanz.buchhaltung.ledger", conformingTo: .utf8PlainText)
	}

	static var buchhaltungReport : UTType
	{
		UTType(importedAs: "belanz.buchhaltung.report", conformingTo: .utf8PlainText)
	}

#if FEATURE_MYEBILANZ
	static var myebilanzReport : UTType
	{
		UTType(importedAs: "belanz.myebilanz.report", conformingTo: .utf8PlainText)
	}

	static var myebilanzReportArchive : UTType
	{
		UTType(importedAs: "belanz.myebilanz.report.archive", conformingTo: .appleArchive)
	}
#endif

#if FEATURE_LATEX
	static var belanzLaTeXReport : UTType
	{
		UTType(importedAs: "belanz.latex.report", conformingTo: .utf8PlainText)
	}
#endif
}
