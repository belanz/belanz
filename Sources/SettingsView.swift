// Copyright 2020-2025 Kai Oezer

import SwiftUI

public struct SettingsView : View
{
	public init()
	{
	}

	public var body : some View
	{
		TabView
		{
			GeneralSettingsView()
				.tabItem{
					Label("settings.general", systemImage: "gearshape")
				}
#if false
			ReportSettingsView()
				.tabItem{
						Label {
							Text(BelanzDocumentSection.report.description)
						} icon: {
							BelanzDocumentSection.report.icon
						}
					}
#endif
		}
		.frame(width: 600, height: 300)
	}
}

// MARK: -

struct GeneralSettingsView : View
{
	var body : some View
	{
		VStack
		{
			Form
			{
				Section(header: Text("settings.general.options_bar.title").bold())
				{
					Toggle("settings.general.options_bar.base_document.always_store_relative_path",
						isOn: BelanzSettings.$alwaysStoreRelativePathToBaseDocument)
						.help("settings.general.options_bar.base_document.always_store_relative_path.help")
				}
			}
			.formStyle(.grouped)
			Spacer()
		}
	}
}

#Preview
{
	GeneralSettingsView()
		.padding()
		.frame(width: 500, height: 400)
}

// MARK: -

struct ReportSettingsView : View
{
	var body : some View
	{
		VStack
		{
			LazyVGrid(
					columns: [GridItem(.fixed(140), alignment: .trailing),
										GridItem(.flexible(), alignment: .leading)],
					alignment: .leading,
					spacing: 14,
					pinnedViews: [])
			{
				HStack
				{
					Text("settings.reports.printing")
						.bold()
					Spacer()
				}
				Color.clear

				Text("settings.reports.printing.latex_path")
				TextField("settings.reports.printing.latex_path.help", text: BelanzSettings.$latexExecutablePath)

				Text("settings.reports.printing.output_folder")
				HStack
				{
					TextField("", text: BelanzSettings.$latexOutputPath)
						.labelsHidden()
					Button{ logger.info("browse for LaTeX output folder") } label: { Text("settings.reports.printing.browse") }
				}
			}

			Spacer()
		}
		.padding([.leading, .trailing], 20)
		.padding([.top, .bottom], 10)
	}
}

#Preview
{
	ReportSettingsView()
		.padding()
		.frame(width: 500, height: 400)
}
