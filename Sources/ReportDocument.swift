// Copyright 2023-2025 Kai Oezer

import SwiftUI
import UniformTypeIdentifiers
import Buchhaltung
import TOMLike
import IssueCollection

struct ReportDocument
{
	let report : Report
}

extension ReportDocument : FileDocument
{
	static var readableContentTypes: [UTType] { [.buchhaltungReport] }
	static var writableContentTypes: [UTType] { [] }

	init(configuration: ReadConfiguration) throws
	{
		guard configuration.contentType == .buchhaltungReport else {
			throw BelanzError.contentTypeNotSupported
		}
		guard let fileContents = configuration.file.regularFileContents else {
			throw CocoaError(.fileReadCorruptFile)
		}
		guard let archivedDocument = String(data: fileContents, encoding: .utf8) else {
			throw BelanzError.documentUnarchiving
		}
		var issues = IssueCollection()
		let report = try TOMLikeCoder.decode(Report.self, from: archivedDocument, issues: &issues)
		assert(issues.issues.isEmpty)
		if !issues.issues.isEmpty { logger.error("Report document decoding issues: \(issues)") }
		self.init(report: report)
	}

	func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper
	{
		throw BelanzError.contentTypeNotSupported
	}
}
