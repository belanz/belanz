// Copyright 2022-2023 Kai Oezer

import SwiftUI
import OrderedCollections

class SectionPickerInfo : ObservableObject
{
	var selectedSection : Binding<BelanzDocumentSection>?
	@Published var sections = OrderedSet<BelanzDocumentSection>()

	init(sections : OrderedSet<BelanzDocumentSection> = [], selectedSection : Binding<BelanzDocumentSection>? = nil)
	{
		self.sections = sections
		self.selectedSection = selectedSection
	}
}
