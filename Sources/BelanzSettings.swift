// Copyright 2020-2024 Kai Oezer

import SwiftUI
import DEGAAP

@MainActor
struct BelanzSettings
{
	@AppStorage("PrintCompanyLogo")
	static var showCompanyLogoOnReport : Bool = true

	@AppStorage("DefaultChartMapping")
	static var defaultChartMapping : DEGAAPMappingSourceChart = .none

	@AppStorage("LatexExecutablPath")
	static var latexExecutablePath : String = ""

	@AppStorage("LatexOutputPath")
	static var latexOutputPath : String = ""

	@AppStorage("AlwaysStoreRelativePathToBaseDocument")
	static var alwaysStoreRelativePathToBaseDocument : Bool = false
}
