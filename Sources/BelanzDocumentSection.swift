//  Copyright 2022 Kai Oezer

import Foundation
import SwiftUI
import OrderedCollections

enum BelanzDocumentSection : Int, Identifiable, CustomStringConvertible
{
	case companyInfo = 1
	case inventory = 2
	case journal = 3
	case report = 4

	static let allSections : OrderedSet<BelanzDocumentSection> = [
		.companyInfo,
		.inventory,
		.journal,
		.report
	]
	static let ledgerSections : OrderedSet<BelanzDocumentSection> = [
		.companyInfo,
		.inventory,
		.journal
	]

	var identifier : String
	{
		switch self
		{
			case .companyInfo: return "section.company"
			case .inventory: return "section.inventory"
			case .journal: return "section.journal"
			case .report: return "section.report"
		}
	}

	var description : String
	{
		tr(self.identifier)
	}

	var id : String { description }

	var icon : Image
	{
		switch self
		{
			case .companyInfo: return Image(systemName: "person.3")
			case .inventory: return Image(systemName: "dollarsign.circle")
			case .journal: return Image(systemName: "book")
			case .report: return Image(systemName: "doc")
		}
	}
}
