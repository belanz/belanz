#!/bin/sh

# Generates a disk image for software distribution.
# _____________________________________________________________________________
# Adapted from 'create-dmg'
# in https://github.com/create-dmg/create-dmg
# 
# Original copyright:
# 
# The MIT License (MIT)
# 
# Copyright (c) 2008-2014 Andrey Tarantsov
# Copyright (c) 2020 Andrew Janke
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# _____________________________________________________________________________

function printUsage()
{
  echo "Usage:\n${0} <version of Belanz to be packaged in a disk image>"
}

function runFinderScript()
{
  local volume_name=$1
  local device_name=$2

  # pausing to work around occasional "Can’t get disk" (-1728) issues  
  sleep 3

  if /usr/bin/osascript arrange_contents.applescript "${volume_name}"; then
    true
  else
    echo >&2 "Failed running Finder script that arranges the disk image layout."
    hdiutil detach "${device_name}"
    exit 1
  fi
}

if [[ $# -lt 1 ]]; then
  echo "The path to an existing folder is needed as input."
  printUsage
  exit 1
fi

if [[ $# -gt 1 ]]; then
  echo "Too many arguments."
  printUsage
  exit 1
fi

version=$1
image_name="Belanz ${version}"
mount_point="/Volumes/${image_name}"
tmp_image_file_name="Belanz-${version}-tmp.dmg"
final_image_file_name="Belanz-${version}.dmg"
disk_image_size=40
disk_image_background="disk_image_background.png"
release_notes_document="Notizen.pdf"

mkdir -p Deployables

# echo the commands to the terminal
#set -o xtrace

hdiutil create -quiet \
  -srcfolder "Exports/${version}" \
  -volname "${image_name}" \
  -fs HFS+ -fsargs "-c c=64,a=16,e=16" \
  -format UDRW \
  -size ${disk_image_size}m \
  -ov \
  "Deployables/${tmp_image_file_name}"

device_name=$(hdiutil attach -readwrite -noverify -noautoopen "Deployables/${tmp_image_file_name}" | grep '^/dev/' | sed 1q | awk '{print $1}')
#echo "Device name: ${device_name}"

# removing all files but the app bundle
find "${mount_point}" -type f -not -path "${mount_point}/Belanz.app/*" -exec rm -f {} +

# copying background image file
if [[ -f "Assets/${disk_image_background}" ]]; then
  mkdir -p "${mount_point}/.background"
  cp "Assets/${disk_image_background}" "${mount_point}/.background/background.png"
fi

# creating a link to the root Applications folder
pushd "${mount_point}" > /dev/null
ln -s -f /Applications Programme
popd > /dev/null

# setting the volume icon
if [[ -f VolumeIcon.icns ]]; then
  cp VolumeIcon.icns "${mount_point}/.VolumeIcon.icns"
  SetFile -c icnC "${mount_point}/.VolumeIcon.icns"
fi

# copying additional files into the image
if [[ -f "Assets/${release_notes_document}" ]]; then
  cp "Assets/${release_notes_document}" "${mount_point}/"
fi

runFinderScript "${image_name}" ${device_name}

# making the contents of the image read-only to others
chmod -Rf go-w "${mount_point}" &> /dev/null || true

# blessing the image root folder to so that it opens automatically upon mount
echo "Blessing the image root folder..."
sudo bless --folder "${mount_point}" --openfolder "${mount_point}"
echo "Blessing done."

# telling the volume that it has a special file attribute
if [[ -f VolumeIcon.icns ]]; then
  SetFile -a C "${mount_point}"
fi

# Delete unnecessary file system events log if possible
rm -rf "${mount_point}/.fseventsd" || true

# ejecting the mounted image
device_busy=16
detach_successful=0
detach_status=$device_busy
while [ $detach_status -eq $device_busy ]
do
  hdiutil detach "${device_name}" > /dev/null
  detach_status=$?
done

if [ $detach_status -ne $detach_successful ]; then
  echo "Failed to eject the temporary disk image."
  exit 1
fi

# creating the final image by compressing the temporary image
hdiutil convert -quiet "Deployables/${tmp_image_file_name}" -format UDZO -imagekey zlib-level=9 -o "Deployables/${final_image_file_name}"
rm -f "Deployables/${tmp_image_file_name}"

exit 0
