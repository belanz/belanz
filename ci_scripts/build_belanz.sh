#!/bin/sh
# This build script is not meant for use with Xcode Cloud.

build_id=`date +%y_%m_%d-%H_%M_%S`
if [[ $# -ge 1 ]]; then
  build_id=${1}
fi

pushd .. > /dev/nulL

echo "Building and archiving Belanz..."
mkdir -p ci_scripts/BuildProducts/${build_id}
archive_path="ci_scripts/BuildProducts/Belanz-${build_id}.xcarchive"
xcrun xcodebuild -quiet archive \
  -scheme "Belanz (macOS)" \
  -destination generic/platform=macOS \
  -derivedDataPath ci_scripts/BuildProducts/${build_id} \
  -archivePath ${archive_path}

echo "Exporting the Belanz app from the archive..."
mkdir -p ci_scripts/Exports/${build_id}
xcrun xcodebuild -quiet -exportArchive \
  -archivePath ${archive_path} \
  -exportPath ci_scripts/Exports/${build_id} \
  -exportOptionsPlist ci_scripts/exportOptions.plist

echo "Cleaning build products..."
rm -rf ci_scripts/BuildProducts/${build_id}

popd > /dev/null

echo "Done."
