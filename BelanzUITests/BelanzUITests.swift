//  Copyright 2023 Kai Oezer

import XCTest

final class BelanzUITests: XCTestCase
{
	override func setUpWithError() throws
	{
		// Put setup code here. This method is called before the invocation of each test method in the class.

		// In UI tests it is usually best to stop immediately when a failure occurs.
		continueAfterFailure = false

		// In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
	}

	override func tearDownWithError() throws
	{
		// Put teardown code here. This method is called after the invocation of each test method in the class.
	}

	func testOpeningInventorySection() throws
	{
		let app = XCUIApplication()
		app.launch()
//		app.open(testDocumentLocation)
//		print(app.debugDescription) // prints XCUIElement tree
		let sectionPicker = app.radioGroups["section-picker"]
		XCTAssertTrue(sectionPicker.waitForExistence(timeout: 2.0))
		let inventorySectionButton = sectionPicker.radioButtons["section.inventory"]
		inventorySectionButton.hover()
		inventorySectionButton.click()
	}

	func testLaunchPerformance() throws
	{
		// This measures how long it takes to launch your application.
		measure(metrics: [XCTApplicationLaunchMetric()])
		{
			XCUIApplication().launch()
		}
	}
}
