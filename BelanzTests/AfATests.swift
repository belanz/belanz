//  Copyright 2020 Kai Oezer

import Foundation
import XCTest
@testable import BuchhaltungUI

final class AfATest : XCTestCase
{
	static let allTests = [
		("test AfA", testAfA)
	]

	func testAfA()
	{
		guard let archiveLocation = Bundle.module.url(forResource: "AfA-AV", withExtension: "csv", subdirectory:"AfA") else { XCTFail("Missing AfA table archive file."); return }
		guard let afa = AfA(from: archiveLocation) else { XCTFail("Could not load AfA table from archive."); return }
		XCTAssertEqual(afa.entries.count, 510)

		let checkTableEntry : (String, UInt8) -> Void = { (name, expectedYears) in
			guard let entry = afa.entries.first(where:{$0.assetType == name}) else { XCTFail("Could not find AfA table entry for the asset type '\(name)'."); return }
			guard let depreciation = entry.depreciationYears else { XCTFail("No depreciation data for asset type '\(name)'."); return }
			XCTAssertEqual(depreciation, expectedYears)
		}
		checkTableEntry("Handy", 5)
		checkTableEntry("Notebooks", 3)
		checkTableEntry("Personenkraftwagen", 6)
		checkTableEntry("Fahrräder", 7)
	}

}
