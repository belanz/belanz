// Copyright 2023-2024 Kai Oezer

import XCTest
@testable import Belanz

final class UtilsTests : XCTestCase
{
	func testRelativePath() throws
	{
		let path1 = URL(filePath: "/data/persons/employees/jennifer.dat")
		let path2 = URL(filePath: "/data/persons/executives/paula.dat")
		let path3 = URL(filePath: "/files/videos/birds/lakeside/swift.mov")
		let path4 = URL(filePath: "/files/videos/birds/../../photos/lakeside/swift.jpg")
		let path5 = URL(filePath: "/files/photos/../videos/birds/lakeside/eagle.mov")

		let relativePath1to2 = try XCTUnwrap(Utils.relativePath(from: path1, to: path2))
		XCTAssertEqual(relativePath1to2, "../executives/paula.dat")
		let relativePath2to1 = try XCTUnwrap(Utils.relativePath(from: path2, to: path1))
		XCTAssertEqual(relativePath2to1, "../employees/jennifer.dat")
		let relativePath1to3 = try XCTUnwrap(Utils.relativePath(from: path1, to: path3))
		XCTAssertEqual(relativePath1to3, "../../../files/videos/birds/lakeside/swift.mov")
		let relativePath3to1 = try XCTUnwrap(Utils.relativePath(from: path3, to: path1))
		XCTAssertEqual(relativePath3to1, "../../../../data/persons/employees/jennifer.dat")
		let relativePath4to5 = try XCTUnwrap(Utils.relativePath(from: path4, to: path5))
		XCTAssertEqual(relativePath4to5, "../../videos/birds/lakeside/eagle.mov")
	}
}
